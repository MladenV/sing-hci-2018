(function () {'use strict';

    var app = angular.module('myApp')
    app.controller('FilesCtrl', FilesCtrl);

    //FilesCtrl.$inject = ['$filter', '$rootScope', '$scope', 'FileUploader', '$http', '$timeout'];
    function FilesCtrl($filter, $rootScope, $scope, FileUploader, $http, $timeout) {
        var vm = this;

        vm.orderByField = 'modified'
        vm.reverseSort  = true;

        vm.fileList = [];

        vm.selectedFile = null;
        vm.selectFile = function(file){
            vm.selectedFile = file;
        };

        vm.searchText = '';
        vm.spin = false;

        vm.loadList = function(){
          vm.spin = true;
          var req = {
              method: "GET",
              url: "/api/files/all"
          };
          $http(req).then(
              function(resp){
                  var lista = [];
                  for(var i in resp.data){
                    var item = resp.data[i];
                    item.old_name = item.name;
                    lista.push(item);
                  }
                  vm.fileList = lista;
                  vm.spin = false;
                  console.log(vm.fileList);
              }, function(err){
                  vm.message = 'error';
              });
        };

        vm.removeFile = function(el){
            var req = {
                method: "GET",
                url: "/api/files/remove?name="+el.name
            };
            $http(req).then(
                function(resp){
                    vm.loadList();
                }, function(err){
                    vm.message = 'error';
                });
          };
  

        vm.init = function(){
            vm.loadList();
            vm.uploader.url = '/api/files/upload/';
        };

        vm.uploader = $scope.uploader = new FileUploader({ url: '/api/files/upload/'});

        // FILTERS

        // a sync filter
        vm.uploader.filters.push({
            name: 'syncFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                console.log('syncFilter');
                return this.queue.length < 10;
            }
        });

        // an async filter
        vm.uploader.filters.push({
            name: 'asyncFilter',
            fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
                console.log('asyncFilter');
                setTimeout(deferred.resolve, 1e3);
            }
        });

        // CALLBACKS
        vm.uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        vm.uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        vm.uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        vm.uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
        };
        vm.uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        vm.uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        vm.uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        vm.uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        vm.uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        vm.uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        vm.uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
            // vm.uploader.clearQueue();
            vm.loadList();
        };

        vm.init();
    }
})();