#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask
from flask import request
from flask import Response
from flask import Flask, abort, send_file
import json

app = Flask(__name__, static_folder='www')

@app.route('/<path:path>')
def static_file(path):
    try:
        return app.send_static_file(path)
    except:
        return "Error"

@app.route('/')
def root():
    return app.send_static_file('index.html')


@app.route('/api/studenti6')
def studenti6():
    res = []
    with open('studenti.csv') as file:
        for line in file:
            delovi = line.split(',')                    
            res.append({
                'index': delovi[0],
                'ime': delovi[1],
                'prezime': delovi[2],
                'godine': int(delovi[3])
                })
    return Response(json.dumps({'studenti': res}), mimetype="application/json")

@app.route('/api/studenti/add', methods=['POST', 'PUT'])
def studenti_add():
    data = request.data
    obj = json.loads(data)
    with open('studenti.csv', 'a') as file:
        file.write("{},{},{},{}\n".format(obj['index'],obj['ime'], obj['prezime'], obj['godine']))
    return Response(json.dumps({'res': 'ok'}), mimetype="application/json")

@app.route('/api/studenti/save', methods=['POST', 'PUT'])
def studenti_save():
    data = request.data
    obj = json.loads(data)
    res = []
    with open('studenti.csv') as file:
        for line in file:
            delovi = line.split(',')
            if delovi[0] == obj['index']:
                res.append(obj)
            else:                   
                res.append({
                    'index': delovi[0],
                    'ime': delovi[1],
                    'prezime': delovi[2],
                    'godine': int(delovi[3])
                    })
    with open('studenti.csv', 'w') as file:
        for el in res:
            file.write("{},{},{},{}\n".format(el['index'],el['ime'], el['prezime'], el['godine']))
    return Response(json.dumps({'res': 'ok'}), mimetype="application/json")

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True, use_reloader=False)
