var app = angular.module('myApp', ['ui.bootstrap']);

app.controller('Primer7', function($http) {
    var vm = this;
    vm.index = '';
    vm.ime = '';
    vm.prezime = '';
    vm.godine = 0;

    vm.lista = [];

    vm.dodaj = function(){
        var obj = {
            'index': vm.index,
            'ime': vm.ime,
            'prezime': vm.prezime,
            'godine': vm.godine
        }

        var req={
            method: 'POST',
            url: '/api/studenti/add',
            data: obj
        }
        $http(req).then(
            function(res){
                vm.lista.push(obj);
            },
            function(res){}
        )
    }

    vm.init = function(){
        var req = {
            method:'GET',
            url:'/api/studenti6'
        }
        $http(req).then(function(res){
            // pozvace se ako je ok
            vm.lista = res.data.studenti;
        } , function(res){
            // pozvace se ako je greska
        });
    }

    vm.init();
});

app.controller('Primer8', function($http) {
    var vm = this;

    vm.lista = [];


    vm.init = function(){
        var req = {
            method:'GET',
            url:'/api/studenti6'
        }
        $http(req).then(function(res){
            // pozvace se ako je ok
            vm.lista = res.data.studenti;
        } , function(res){
            // pozvace se ako je greska
        });
    }

    vm.select = function(el){
        
    }

    vm.saveEl = function(el){
        var req={
            method: 'POST',
            url: '/api/studenti/save',
            data: el
        }
        $http(req).then(
            function(res){
                vm.init();
            },
            function(res){}
        )
    }

    vm.init();
});
