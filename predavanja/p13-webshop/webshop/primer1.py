x = 4
y = 7
c = x+y
print(c)

niz = [1, 2, 3, 4, 5]

suma = 0
for el in niz:
    suma += el
print(suma)

najveci = None
for el in niz:
    if najveci is None or el>najveci:
        najveci = el
print(najveci)

niz2 = range(20)

print(niz2[0])

print(niz2[19])

print(niz2[-1])

print(niz2[:5])
for i in range(90, 100):
    print(i)

d = 'Danas nam je divan dan'

for el in d:
    print(el)

f = d[:10]
print(f)

parts = d.split(' ')
print(parts)

g = 'ime,prezime,index'
parts = g.split(',')
for el in parts:
    print(el)


# with open('proba.txt', 'w') as file:
#    file.write("Danas nam je divan dan\n")

with open('proba.txt') as file:
    for line in file:
        line = line[:-1]  # 'ime,prezime,index\n'
        delovi = line.split(',')
        print(delovi[1])

c = 0
with open('hobit.txt') as f:
    for line in f:
        c += 1
print(c)

def process(path):
    words = {}
    with open(path) as f:
        for line in f:
            for c in line:
                if c not in words:
                    words[c] = 1
                else:
                    words[c] += 1
    print(words['a'])
    for key, value in sorted(words.iteritems(), key=lambda(k,v): (v,k)):
    print("%s: %s" % (key, value))

process('hobit.txt')

