var app = angular.module('myApp', ['ui.bootstrap']);
app.controller('MyCtrl', function($scope, $window, $timeout) {
    var vm = this;
    vm.a = 10;
    vm.b = 20;
});

app.controller('Primer2', function($scope, $window, $timeout) {
    var vm = this;
    vm.a = 10;
    vm.b = 20;
    vm.c = 0;

    vm.racunaj = function(){
        vm.c = vm.a*vm.b;
    }
});

app.controller('Primer3', function() {
    var vm = this;
    vm.a = 0;
    vm.lista = [];
    vm.c = 0;

    vm.dodaj = function(){
        vm.lista.push(vm.a);
        var najveci = vm.a;
        for(var i in vm.lista){
            if(vm.lista[i]>najveci){
                najveci = vm.lista[i];
            }
        }
        vm.c = najveci;
    }
});

app.controller('Primer4', function() {
    var vm = this;
    vm.ime = '';
    vm.prezime = '';
    vm.godine = 0;

    vm.lista = [];
    vm.prosekGodina = 0;

    vm.dodaj = function(){
        var obj = {
            'ime': vm.ime,
            'prezime': vm.prezime,
            'godine': vm.godine
        }

        vm.lista.push(obj);
        var suma = 0;
        for(var i in vm.lista){
            suma += vm.lista[i].godine;
        }
        vm.prosekGodina = suma/vm.lista.length;
    }
});


