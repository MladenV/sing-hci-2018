var app = angular.module('myApp', ['ui.bootstrap']);

app.factory('UserService', function() {
   var vm  = this;

    vm.login  = function(username, password){

        if(vm.onLogin){
            vm.onLogin();
        }
        return true;
    }

   return vm;
});

app.controller('LoginCtrl', function($http, UserService) {
    var vm = this;
    vm.username = "";
    vm.password = "";
    vm.isLogged = false;

    vm.login = function(){
        vm.isLogged = UserService.login(vm.username, vm.password);
    }
});

app.controller('Primer8', function($http, UserService) {
    var vm = this;

    vm.lista = [];
    vm.isLogged = false;

    vm.init = function(){
        var req = {
            method:'GET',
            url:'/api/studenti6'
        }
        $http(req).then(function(res){
            // pozvace se ako je ok
            vm.lista = res.data.studenti;
        } , function(res){
            // pozvace se ako je greska
        });
    }

    vm.select = function(el){
        
    }

    vm.saveEl = function(el){
        var req={
            method: 'POST',
            url: '/api/studenti/save',
            data: el
        }
        $http(req).then(
            function(res){
                vm.init();
            },
            function(res){}
        )
    }

    // vm.init();
    UserService.onLogin = function(){
        vm.init();
        vm.isLogged = true;    
    }
});
