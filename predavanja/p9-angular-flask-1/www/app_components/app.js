var app = angular.module('myApp', ['ui.bootstrap']);

app.controller('Primer5', function($http) {
    var vm = this;
    vm.ime = '';
    vm.prezime = '';
    vm.godine = 0;

    vm.lista = [];
    vm.prosekGodina = 0;

    vm.dodaj = function(){
        var obj = {
            'ime': vm.ime,
            'prezime': vm.prezime,
            'godine': vm.godine
        }

        vm.lista.push(obj);
        var suma = 0;
        for(var i in vm.lista){
            suma += vm.lista[i].godine;
        }
        vm.prosekGodina = suma/vm.lista.length;
    }

    vm.init = function(){
        var req = {
            method:'GET',
            url:'/api/studenti'
        }
        $http(req).then(function(res){
            // pozvace se ako je ok
            vm.lista = res.data.studenti;
        } , function(res){
            // pozvace se ako je greska
        });
    }

    vm.init();
});

app.controller('Primer6', function($http) {
    var vm = this;
    vm.ime = '';
    vm.prezime = '';
    vm.godine = 0;

    vm.lista = [];
    vm.prosekGodina = 0;

    vm.init = function(){
        var req = {
            method:'GET',
            url:'/api/studenti6'
        }
        $http(req).then(function(res){
            // pozvace se ako je ok
            vm.lista = res.data.studenti;
        } , function(res){
            // pozvace se ako je greska
        });
    }

    vm.init();
});