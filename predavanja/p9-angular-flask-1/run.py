#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask
from flask import request
from flask import Response
from flask import Flask, abort, send_file
import json

app = Flask(__name__, static_folder='www')

@app.route('/<path:path>')
def static_file(path):
    try:
        return app.send_static_file(path)
    except:
        return "Error"

@app.route('/')
def root():
    return app.send_static_file('index.html')

@app.route('/api/studenti')
def studenti():
    res = []
    res.append({
        'ime': 'pera',
        'prezime': 'petrovic',
        'godine':30
        })
    return Response(json.dumps({'studenti': res}), mimetype="application/json")

@app.route('/api/studenti6')
def studenti6():
    res = []
    with open('studenti.csv') as file:
        for line in file:
            delovi = line.split(',')                    
            res.append({
                'ime': delovi[0],
                'prezime': delovi[1],
                'godine': int(delovi[2])
                })
    return Response(json.dumps({'studenti': res}), mimetype="application/json")


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True, use_reloader=False)
