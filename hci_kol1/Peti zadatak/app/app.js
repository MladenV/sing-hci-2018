(function (angular){
var app = angular.module("PetaAplikacija", []);

app.controller("ValidationCtrl", function() {
    var vc = this
    //validacija za lozinku
    vc.plozinkaValidation = (function(){
        if(vc.lozinka == vc.plozinka){
            vc.pvalid = true;
        }else{
            vc.pvalid = false;
        }
        if(vc.lozinka && vc.lozinka.length < 8){
            vc.pvalid = false;
        }
        //regularni izraz za proveru brojeva i velikih slova
        var regExp = /(?=(.*\d){2})([A-Z].*){2}/;
        if(!regExp.test(vc.lozinka)){
            vc.pvalid = false;
        }


    });
    //validacija datuma
    vc.dateValidation = (function(){
        var ageDif = Date.now() - vc.datum.getTime();
        var ageDate = new Date(ageDif);
        if(Math.abs(ageDate.getUTCFullYear() - 1970) < 18){
            vc.dvalid = false;
        }else{
            vc.dvalid = true;
        }
    });

});

})(angular);