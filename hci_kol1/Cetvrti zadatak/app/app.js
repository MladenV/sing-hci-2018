(function (angular){
var app = angular.module("CetvrtaAplikacija", []);

app.controller("ZaposleniCtrl", function() {
    var zc = this;
    zc.employees = [
        {name: "Marko Markovic", date: new Date("1992.11.25"), salary:70000},
        {name: "Joseph Joestar", date: new Date("1925.10.05"), salary:89500},
        {name: "Kakoyin....", date: new Date("1978.02.24"), salary:50000}
    ];

    average();

    function average() {
        var total  = 0;
        zc.employees.forEach(p => {
            total += (p.salary);
        });
        zc.average = total / zc.employees.length;
    }

    zc.removeEmployee = (function(i) {
        zc.employees.splice(i, 1);
        average();
    });

    zc.addEmployee = (function() {
        zc.employees.push(zc.newEmployee);
        zc.newEmployee = {name: "", date: new Date(), salary: 0};
        average();
    });

    zc.newEmployee = {name: "", date: new Date(), salary: 0};
});

})(angular);