(function (angular){
var app = angular.module("PrvaAplikacija", []);

app.controller("KonverzijaCtrl", function() {
    var kor = this;
    kor.celzijus = 0;
    kor.kelvin = 0;

    kor.konvertuj = (function(){
        kor.kelvin = kor.celzijus + 273;
    });
});

})(angular);