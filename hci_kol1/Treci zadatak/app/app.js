(function (angular){
var app = angular.module("TrecaAplikacija", []);

app.controller("ESPBCtrl", function() {
    var ec = this
    var minpts = 38;
    var minptsb = 48;
    var maxpts = 60;
    var miny = 0;
    var maxy = 4;

    ec.god = 1;
    ec.pts = 0;
    ec.response = ""

    ec.izracunaj = (function(){
        //provera da li je unesena nevalidna godina
        if(ec.god < miny || ec.god > maxy ){
            ec.response = "Nevalidna godina";
            return;
        }
        //najveci i najmanji dozvoljen broj bodova
        var ptsfloor = (ec.god - 1) * minpts;
        var ptsceil = ec.god * maxpts;

        //provera da li je unesen broj bodova van opsega
        if(ec.pts < ptsfloor || ec.pts > ptsceil){
            ec.response = "Nevalidan broj bodova";
            return;
        }
        //ukoliko je 4. godina, sa validnim brojem bodova, ispisuje se posebna poruka
        if(ec.god == 4){
            var points = 240 - ec.pts;
            ec.response = "Potrebno vam je " + points + " bodova do završetka studija.";
            return;
        }

        //odredjivanje koliko je bodova potrebno, i ukoliko je negativan broj, postavlja se na 0
        var samof = (minpts * ec.god - ec.pts > 0 ? minpts * ec.god - ec.pts : 0);
        
        var bud = (minptsb * ec.god - ec.pts > 0 ? minptsb * ec.god - ec.pts : 0);



        ec.response = "Potrebno je " + samof + " bodova za samofinansiranje i " + bud  + " bodova za budzet";


    });

});

})(angular);