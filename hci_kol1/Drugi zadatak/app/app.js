(function (angular){
var app = angular.module("DrugaAplikacija", []);

app.controller("PravougaonikCtrl", function() {
    var pr = this;
    pr.a = 0;
    pr.b = 0;

    pr.o = 0;
    pr.p = 0;
    pr.d = 0;

    pr.obim = (function(){
        pr.o = 2 * pr.a + 2 * pr.b;
    });

    pr.povrsina = (function(){
        pr.p = pr.a * pr.b;
    });

    pr.dijagonala = (function(){
        pr.d = Math.sqrt(Math.pow(pr.a, 2) + Math.pow(pr.b, 2));
    });

    pr.izracunaj = (function(){
        pr.obim();
        pr.povrsina();
        pr.dijagonala();
    });

});

})(angular);