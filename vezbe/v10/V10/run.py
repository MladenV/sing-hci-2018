#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask
from flask import request
from flask import Response
from flask import Flask, abort, send_file
import json

app = Flask(__name__, static_folder='www', root_path='')

@app.route('/<path:path>')
def static_file(path):
    try:
        return app.send_static_file(path)
    except:
        return "Error"

@app.route('/')
def root():
    return app.send_static_file('index.html')


@app.route('/api/proizvodi')
def proizvodi6():
    res = []
    with open('proizvodi.csv') as file:
        for line in file:
            delovi = line.split(',')                    
            res.append({
                'id': delovi[0],
                'naziv': delovi[1],
                'cena': delovi[2],
                'kolicina': int(delovi[3])
                })
    return Response(json.dumps({'proizvodi': res}), mimetype="application/json")

@app.route('/api/proizvodi/add', methods=['POST', 'PUT'])
def proizvodi_add():
    data = request.data
    obj = json.loads(data)
    with open('proizvodi.csv', 'a') as file:
        file.write("{},{},{},{}\n".format(obj['id'],obj['naziv'], obj['cena'], obj['kolicina']))
    return Response(json.dumps({'res': 'ok'}), mimetype="application/json")

@app.route('/api/proizvodi/save', methods=['POST', 'PUT'])
def proizvodi_save():
    data = request.data
    obj = json.loads(data)
    res = []
    with open('proizvodi.csv') as file:
        for line in file:
            delovi = line.split(',')
            if delovi[0] == obj['id']:
                res.append(obj)
            else:                   
                res.append({
                    'id': delovi[0],
                    'naziv': delovi[1],
                    'cena': delovi[2],
                    'kolicina': int(delovi[3])
                    })
    with open('proizvodi.csv', 'w') as file:
        for el in res:
            file.write("{},{},{},{}\n".format(el['id'],el['naziv'], el['cena'], el['kolicina']))
    return Response(json.dumps({'res': 'ok'}), mimetype="application/json")

@app.route('/api/proizvodi/delete', methods=['DELETE'])
def proizvodi_delete():
    data = request.data
    obj = json.loads(data)
    res = []
    with open('proizvodi.csv') as file:
        for line in file:
            delovi = line.split(',')
            if delovi[0] == obj['id']:
                pass
            else:                   
                res.append({
                    'id': delovi[0],
                    'naziv': delovi[1],
                    'cena': delovi[2],
                    'kolicina': int(delovi[3])
                    })
    with open('proizvodi.csv', 'w') as file:
        for el in res:
            file.write("{},{},{},{}\n".format(el['id'],el['naziv'], el['cena'], el['kolicina']))
    return Response(json.dumps({'res': 'ok'}), mimetype="application/json")

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True, use_reloader=False)
