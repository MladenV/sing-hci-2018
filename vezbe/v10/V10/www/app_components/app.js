var app = angular.module('myApp', ['ui.bootstrap']);

app.controller('ProizvodiCtrl', function($http) {
    var vm = this;
    vm.id = '';
    vm.naziv = '';
    vm.cena = 0;
    vm.kolicina = 0;

    vm.lista = [];

    

    vm.init = function(){
        var req = {
            method:'GET',
            url:'/api/proizvodi'
            //data, ukoliko se salju neki podaci
        }
        $http(req).then(function(res){
            // pozvace se ako je ok
            vm.lista = res.data.proizvodi;
        } , function(res){
            // pozvace se ako je greska
        });
    }

    vm.init();
});

