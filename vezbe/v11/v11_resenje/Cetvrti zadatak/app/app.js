(function (angular){
var app = angular.module("CetvrtaAplikacija", []);

app.controller("StudentiCtrl", function() {
    var st = this;
    //lista studenata
    st.studenti = [
        {name: "Marko Markovic", ind: "SL1/2002", avg:8.15},
        {name: "Petar Petrovic", ind: "SL61/1995", avg:9.12},
        {name: "Milan Milanovic", ind: "MA17/2", avg:7.00}
    ];

    //na pocetku odmah pozivamo funkciju za racunanje max proseka, da bi se odmah izracunala kad se ucita stranica
    max_avg();

    //funkcija za racunanje najveceg proseka
    //pocetni maksimum je 0
    //zatim prolazimo kroz sve studente u listi studenata i poredimo da li je njihov prosek veci od trenutnog
    //ukoliko jeste, on postaje novi najveci prosek
    function max_avg() {
        st.total  = 0;
        st.studenti.forEach(p => {
            if(p.avg > st.total){
                st.total = p.avg;
            }
        });
    }
    //funkcija za uklanjanje studenata, pozivamo funkciju splice(i, j) za uklanjanje iz liste
    //i predstavlja pocetni indeks, odnosno mesto od kojeg pocinje uklanjanje iz liste
    //j predstavlja broj elemenata koji se uklanjaju, pocevsi od i. (npr kod nas 1, jer samo uklanjamo jednog studenta)
    st.removeStudent = (function(i) {
        st.studenti.splice(i, 1);
        max_avg();
    });

    //funkcija za dodavanje novog studenta, preuzima se student iz forme za dodavanje studenata, push-uje u listu
    //zatim se prazni forma, postavljanjem polja na prazan string, odnosno 0
    st.addStudent = (function() {
        st.studenti.push(st.newStudent);
        st.newStudent = {name: "", ind: "", avg:0};
        max_avg();
    });
    //objekat koji predstavlja novog studenta, koji ce biti dodavan u listu
    st.newStudent = {name: "", ind: "", avg:0};
});

})(angular);