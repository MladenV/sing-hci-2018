(function (angular){
var app = angular.module("TrecaAplikacija", []);

app.controller("KalkulatorCtrl", function() {
    var ctrl = this;
    //inicijalizacija promenljivih
    ctrl.prvi_broj = 0;
    ctrl.drugi_broj = 0;
    ctrl.rezultat = 0;
    //definicija funkcija za kalkulator
    ctrl.sabiranje = (function(){
        ctrl.rezultat = ctrl.prvi_broj + ctrl.drugi_broj;
    });

    ctrl.oduzimanje = (function(){
        ctrl.rezultat = ctrl.prvi_broj - ctrl.drugi_broj;
    });

    ctrl.mnozenje = (function(){
        ctrl.rezultat = ctrl.prvi_broj * ctrl.drugi_broj;
    });

    ctrl.deljenje = (function(){
        ctrl.rezultat = ctrl.prvi_broj / ctrl.drugi_broj;
    });
});

})(angular);