(function (angular){
var app = angular.module("DrugaAplikacija", []);

app.controller("KvadarCtrl", function() {
    var kctr = this;
    //incijalizacija promenljivih koje nam trebaju
    kctr.a = 0;
    kctr.b = 0;
    kctr.c = 0;
    kctr.zapremina = 0;
    //funkcija za izracunavanje zapremine kvadra
    kctr.izracunaj = (function(){
       kctr.zapremina = kctr.a * kctr.b * kctr.c;  
    });
});

})(angular);