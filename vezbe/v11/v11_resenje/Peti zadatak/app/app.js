(function (angular){
var app = angular.module("PetaAplikacija", []);

app.controller("PremijaCtrl", function() {
    var ctrl = this;
    ctrl.premija = 5000;

    //funkcija za izracunavanje premije
    ctrl.izracunajPremiju = (function(){
        //ponovo podesavamo osnovnu premiju, inace bismo uvek menjali istu promenljivu
        ctrl.premija = 5000;
        //provera za pol iz radio button-a
        if(ctrl.pol == "z"){
            ctrl.premija += 500;
        }
        //provera za godine
        var ageDif = Date.now() - ctrl.datum.getTime();
        var ageDate = new Date(ageDif);
        if((Math.abs(ageDate.getUTCFullYear() - 1970) > 20) && (Math.abs(ageDate.getUTCFullYear() - 1970) < 50)){
            ctrl.premija -=1500;
        }

    });


});

})(angular);