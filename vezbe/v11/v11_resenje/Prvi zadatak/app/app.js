(function (angular){
var app = angular.module("PrvaAplikacija", []);

app.controller("KonverzijaCtrl", function() {
    var kor = this;
    kor.rsd = 0;
    kor.eur = 0;

    kor.konvertuj = (function(){
        kor.eur = kor.rsd / 119;
    });
});

})(angular);