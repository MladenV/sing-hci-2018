var app = angular.module('myApp', ['ui.bootstrap']);
app.controller('Primer1', function($scope, $window, $timeout) {
    var vm = this;
    vm.a = 10;
    vm.b = 20;
    vm.poruka = "";

    vm.izracunaj = function(){
        if(vm.a>vm.b)
            vm.poruka = "veci je prvi";
        else
            vm.poruka = "veci je drugi";
    }

});

app.controller('Primer2', function($scope, $window, $timeout) {
    var vm = this;
    vm.ime = "";
    vm.lozinka = "";
    vm.ulogovan = false;
    vm.poruka = "";


    vm.login = function(){
        if(vm.ime == "pera" && vm.lozinka == "1234"){
            vm.poruka = "Korisnik pera je ulogovan";
            vm.ulogovan = true;
        }else{
            vm.poruka = "Pogresili ste lozinku ili korisnicko ime";
            vm.ulogovan = false;
        }
    }

    vm.logout = function(){
        vm.ulogovan = false;
        vm.ime = "";
        vm.lozinka = "";
        vm.poruka = "Hvala na ...";
    }

});


app.controller('Primer3', function($scope, $window, $timeout) {
    var vm = this;
    vm.ime = "";
    vm.plata = "";

    vm.zaposleni = [];

    vm.prosek = 0;


    vm.dodaj = function(){
        var obj = {
            ime: vm.ime,
            plata: vm.plata
        }
        vm.zaposleni.push(obj);
        vm.izracunajProsek();
    }

    vm.izracunajProsek = function(){
        var suma = 0;
        for(var i in vm.zaposleni){
            var mm = vm.zaposleni[i];
            suma += mm.plata;
        }
        if(vm.zaposleni.length>0){
            vm.prosek = suma/vm.zaposleni.length;
        }
    }

});
