var app = angular.module('myApp', ['ui.bootstrap']);
app.controller('MyCtrl', function($scope, $window, $timeout) {
    var vm = this;
    vm.a = 10;
    vm.b = 20;
});

app.controller('Zadatak2Ctrl', function($scope, $window, $timeout) {
    var vm = this;
    vm.a = 10;
    vm.b = 20;
    vm.c = 0;

    vm.izracunaj = function(){
        vm.c = vm.a*vm.b;
    }
});

app.controller('Zadatak3Ctrl', function($scope, $window, $timeout) {
    var vm = this;
    vm.niz = [0,0,0,0,0];

    vm.c = 0;

    vm.izracunaj = function(){
        var lista = [];
        for(var i in vm.niz){
            if(vm.niz[i]>0)
                lista.push(vm.niz[i])
        }

        vm.c = lista[0];
        // if(vm.niz[1]>vm.c)
        //     vm.c = vm.niz[1];
        // if(vm.niz[2]>vm.c)
        //     vm.c = vm.niz[2];
        // if(vm.niz[3]>vm.c)
        //     vm.c = vm.niz[3];
        // if(vm.niz[4]>vm.c)
        //     vm.c = vm.niz[4];

        for(var i in lista){
            if(lista[i]<vm.c)
                vm.c = lista[i];
        }

    }
});


app.controller('Zadatak4Ctrl', function($scope, $window, $timeout) {
    var vm = this;
    vm.niz = [0,0,0,0,0];

    vm.c = 0;

    vm.izracunaj = function(){
        var lista = [];
        for(var i in vm.niz){
            if((vm.niz[i]%2 == 0) && (vm.niz[i]>0))
                lista.push(vm.niz[i])
        }

        vm.c = lista[0];
        for(var i in lista){
            if(lista[i]<vm.c)
                vm.c = lista[i];
        }
    }
});

app.controller('Zadatak5Ctrl', function($scope, $window, $timeout) {
    var vm = this;

    vm.obj = {
        proizvod: "",
        cena: 0
    }

    vm.niz = [];
    vm.suma = 0;

    vm.provera = function(){
        if(vm.obj.cena>18)
            return true;
        else
            return false;
    }

    vm.dodaj = function(){
        if(vm.provera()){
            vm.niz.push(
                {
                    proizvod: vm.obj.proizvod,
                    cena: vm.obj.cena        
                })
        }
        vm.izracunaj();      
    }

    vm.izracunaj = function(){
        vm.suma = 0;
        for(var i in vm.niz){
            vm.suma += vm.niz[i].cena;
        }
    }
});